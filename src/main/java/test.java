import javax.sound.midi.Soundbank;
import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.io.*;
import java.math.BigDecimal;
import java.net.Socket;
import java.sql.SQLOutput;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class test
{
    public static final String RED = (char)27+"[91m";
    public static final String GREEN = (char)27+"[92m";
    public static final String BLACK = (char)27+"[m";

    public static void main(String[] args)
    {
        List<String> x = new ArrayList<>();
        x.add("2");

        List<String> y = null;


        List<String> z = new ArrayList<>();
        z.addAll(x);

        if(y!=null)
            z.addAll(y);

        Set<String> set = new HashSet<>(z);
        z.clear();
        z.addAll(set);


        //System.out.println(z);

        List<String> w = new ArrayList<>();
        Set<String> set1 = new HashSet<>(w);
        w.clear();
        w.addAll(set1);
        //System.out.println(w);

        LocalTime now = LocalTime.now();

        //System.out.println("time: "+now.getHour()+":"+now.getMinute());

        String IBAN = "SA4720EC0208992000000935";
        //System.out.println(IBAN.substring(IBAN.length()-13));

        //System.out.println(RED+ "ERROR MESSAGE IN RED"+BLACK);

        String code = "000201" +
                "010212" +
                "2930" +
                    "0012D15600000000" +
                    "0510A93FO3230Q" +
                "3128" +
                    "0012D15600000001" +
                    "030812345678" +
                "52044111" +
                "5802CN" +
                "5914BEST TRANSPORT" +
                "6007BEIJING" +
                "6420" +
                    "0002ZH" +
                    "0104最佳运输" +
                    "0202北京" +
                "540523.72" +
                "5303156" +
                "550201" +
                "6233" +
                    "03041234" +
                    "0603***" +
                    "0708A6008667" +
                    "0902ME" +
                "9132" +
                    "0016A011223344998877" +
                    "070812345678" +
                "6304A13A";

        EMVQRParser parser = new EMVQRParser(code);
        System.out.println(parser.getTransactionAmount());
    }

    public static Integer findMin(List<Integer> list)
    {

        // check list is empty or not
        if (list == null || list.size() == 0) {
            return Integer.MAX_VALUE;
        }

        // create a new list to avoid modification
        // in the original list
        List<Integer> sortedlist = new ArrayList<>(list);

        // sort list in natural order
        Collections.sort(sortedlist);
        System.out.println(sortedlist);

        // first element in the sorted list
        // would be minimum
        return sortedlist.get(0);
    }

    // function return maximum value in an unsorted
    // list in Java using Collection
    public static Integer findMax(List<Integer> list)
    {

        // check list is empty or not
        if (list == null || list.size() == 0) {
            return Integer.MIN_VALUE;
        }

        // create a new list to avoid modification
        // in the original list
        List<Integer> sortedlist = new ArrayList<>(list);

        // sort list in natural order
        Collections.sort(sortedlist);

        // last element in the sorted list would be maximum
        return sortedlist.get(sortedlist.size() - 1);
    }
    public static String dateTimeFormatter(LocalDateTime dateTime)
    {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy'T'HH:mm:ss");
        try
        {
            return format.format(dateTime);
        }catch (Exception e)
        {
            e.printStackTrace();
            return dateTime+"";
        }
    }

    public static LocalDateTime convertDateToLocalDateTime(String strDate)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        LocalDateTime dateTime = null;
        try {
            Date date = new Date(strDate);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Date entered could not be Parsed");
        }
            //date = dateFormat.parse(strDate);
            //strDate = dateFormat.format(date);
            dateTime = LocalDateTime.parse(strDate+" "+LocalTime.of(0,0,0).format(timeFormatter), formatter);


        return dateTime;
    }

    public static File createTxtFile(String fileName)
    {
        Writer writer = null;
        File file = null;
        try {
            file = new File(fileName);
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file), "utf-8"));
            writer.write("Something lol");
        } catch (IOException ex) {
            // Report
        } finally {
            try {writer.close();} catch (Exception ex) {/*ignore*/}
        }
        return file;
    }

    public static void readTxtFile(File file)
    {
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while((line = br.readLine()) != null){
                //process the line
                System.out.println(line);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
