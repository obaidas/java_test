import java.util.Scanner;
import java.util.regex.Pattern;

public class regexTest
{
    public static final String RED = (char)27+"[91m";
    public static final String GREEN = (char)27+"[92m";
    public static final String BLACK = (char)27+"[m";

    public static void main(String [] args)
    {
        System.out.println("Enter Email (0 to exit):");
        Scanner scanner = new Scanner(System.in);
        String email = scanner.nextLine();

        while (!email.equalsIgnoreCase( "0"))
        {
            if(isEmailCorrect(email))
                System.out.println("\n" + GREEN +"Correct Email" + BLACK);
            else
                System.out.println("\n" + RED +"Wrong Email" + BLACK);

            System.out.println("\nEnter Email (0 to exit):");
            email = scanner.nextLine();
        }
    }

    public static boolean isEmailCorrect(String email)
    {
        String emailRegex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"" +
                "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@" +
                "(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[" +
                "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:" +
                "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        if(Pattern.compile(emailRegex).matcher(email).matches())
            return true;
        else
            return false;
    }
}
