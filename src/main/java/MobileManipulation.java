import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

public class MobileManipulation
{
    public static void main(String[] args)
    {
        String walletCode = "966599121868";
        String ValidMobile = StandardPhoneFormat("966", walletCode);
        if (ValidMobile.startsWith("0_"))
            walletCode = ValidMobile.substring(2);
        else
            walletCode = "0"+ValidMobile.substring(4);

        System.out.println(ValidMobile);
        System.out.println(walletCode);
    }

    public static String StandardPhoneFormat(String countryCode, String number)
    {
        try {
            PhoneNumberUtil util  = PhoneNumberUtil.getInstance();
            int parsedCountryCode = Integer.parseInt(countryCode);
            Phonenumber.PhoneNumber parsedNumber = util.parse(number,
                    util.getRegionCodeForCountryCode(parsedCountryCode));

            boolean validationFlag = PhoneNumberUtil.getInstance().isValidNumber(parsedNumber);

            if(!validationFlag){
                throw new Exception();
            }
            return util.format(parsedNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "0_"+ex.getMessage();
        }
    }
}
