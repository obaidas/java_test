
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EMVQRParser
{
    private String code;
    private String payloadVersion;
    private String pointOfInitiationID;
    private String pointOfInitiationIDType;
    private String CRC;
    private Double transactionAmount;
    private String transactionCurrency;
    private String tipConvenienceIndicator;
    private Double fixedFeeValue;
    private Double percentageFeeValue;
    private Map<String,String> merchantInfo = new HashMap<>();
    private Map<String,String> merchantGUI = new HashMap<>();
    private Map<String,String> merchantAccount = new HashMap<>();
    private String consumerData;
    private String MCC;
    private String countryCode;
    private String merchantName;
    private String merchantCity;
    private String language;
    private String merchantNameWithLanguage;
    private String merchantCityWithLanguage;
    private Integer postalCode;
    private String mobileNumber;
    private String transactionPurpose;
    private String referenceLabel;
    private String storeLabel;
    private String customerLabel;
    private String terminalLabel;
    private String loyaltyNumber;
    private String billNumber;
    private final int lengthStartPosition = 4;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPayloadVersion() {
        return payloadVersion;
    }

    public void setPayloadVersion(String payloadVersion) {
        this.payloadVersion = payloadVersion;
    }

    public String getPointOfInitiationID() {
        return pointOfInitiationID;
    }

    public void setPointOfInitiationID(String pointOfInitiationID) {
        this.pointOfInitiationID = pointOfInitiationID;
    }

    public String getPointOfInitiationIDType() {
        return pointOfInitiationIDType;
    }

    public void setPointOfInitiationIDType(String pointOfInitiationIDType) {
        this.pointOfInitiationIDType = pointOfInitiationIDType;
    }

    public String getCRC() {
        return CRC;
    }

    public void setCRC(String CRC) {
        this.CRC = CRC;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    public String getTipConvenienceIndicator() {
        return tipConvenienceIndicator;
    }

    public void setTipConvenienceIndicator(String tipConvenienceIndicator) {
        this.tipConvenienceIndicator = tipConvenienceIndicator;
    }

    public Double getFixedFeeValue() {
        return fixedFeeValue;
    }

    public void setFixedFeeValue(Double fixedFeeValue) {
        this.fixedFeeValue = fixedFeeValue;
    }

    public Double getPercentageFeeValue() {
        return percentageFeeValue;
    }

    public void setPercentageFeeValue(Double percentageFeeValue) {
        this.percentageFeeValue = percentageFeeValue;
    }

    public Map<String, String> getMerchantInfo() {
        return merchantInfo;
    }

    public void setMerchantInfo(Map<String, String> merchantInfo) {
        this.merchantInfo = merchantInfo;
    }

    public Map<String, String> getMerchantGUI() {
        return merchantGUI;
    }

    public void setMerchantGUI(Map<String, String> merchantGUI) {
        this.merchantGUI = merchantGUI;
    }

    public Map<String, String> getMerchantAccount() {
        return merchantAccount;
    }

    public void setMerchantAccount(Map<String, String> merchantAccount) {
        this.merchantAccount = merchantAccount;
    }

    public String getConsumerData() {
        return consumerData;
    }

    public void setConsumerData(String consumerData) {
        this.consumerData = consumerData;
    }

    public String getMCC() {
        return MCC;
    }

    public void setMCC(String MCC) {
        this.MCC = MCC;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantCity() {
        return merchantCity;
    }

    public void setMerchantCity(String merchantCity) {
        this.merchantCity = merchantCity;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMerchantNameWithLanguage() {
        return merchantNameWithLanguage;
    }

    public void setMerchantNameWithLanguage(String merchantNameWithLanguage) {
        this.merchantNameWithLanguage = merchantNameWithLanguage;
    }

    public String getMerchantCityWithLanguage() {
        return merchantCityWithLanguage;
    }

    public void setMerchantCityWithLanguage(String merchantCityWithLanguage) {
        this.merchantCityWithLanguage = merchantCityWithLanguage;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getTransactionPurpose() {
        return transactionPurpose;
    }

    public void setTransactionPurpose(String transactionPurpose) {
        this.transactionPurpose = transactionPurpose;
    }

    public String getReferenceLabel() {
        return referenceLabel;
    }

    public void setReferenceLabel(String referenceLabel) {
        this.referenceLabel = referenceLabel;
    }

    public String getStoreLabel() {
        return storeLabel;
    }

    public void setStoreLabel(String storeLabel) {
        this.storeLabel = storeLabel;
    }

    public String getCustomerLabel() {
        return customerLabel;
    }

    public void setCustomerLabel(String customerLabel) {
        this.customerLabel = customerLabel;
    }

    public String getTerminalLabel() {
        return terminalLabel;
    }

    public void setTerminalLabel(String terminalLabel) {
        this.terminalLabel = terminalLabel;
    }

    public String getLoyaltyNumber() {
        return loyaltyNumber;
    }

    public void setLoyaltyNumber(String loyaltyNumber) {
        this.loyaltyNumber = loyaltyNumber;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public EMVQRParser(String code)
    {
        this.code = code;

        try {
            parseCode(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseCode(String code) throws Exception
    {
        if (code.isEmpty() || code.length() > 512)
            throw new Exception("QR Code Length Exceeded");

        if(!code.startsWith("000201"))
            throw new Exception("Payload should start with 000201");

        payloadVersion = "01";
        this.code = code.substring(6);

        while(!this.code.isEmpty())
            fillDataBasedOnID(this.code);
    }

    private String fillDataBasedOnID(String payload) throws Exception
    {
        String id = getID(payload);
        int payloadLength = getLength(payload);

        switch(id)
        {
            case "01": // Point of Initiation

                payloadLength = 6;

                this.pointOfInitiationID = payload.substring(lengthStartPosition,payloadLength);

                if(pointOfInitiationID.equalsIgnoreCase("11"))
                    this.pointOfInitiationIDType = "Static QR";
                else if (pointOfInitiationID.equalsIgnoreCase("12"))
                    this.pointOfInitiationIDType = "Dynamic QR";

                break;
            //Case from ID 29 to 51 & "91" Merchant Account
            case "26":case "27":case "28":case "29":case "30": case "31":case "32":case "33":case "34":case "35":case "36":case "37":case "38":
            case "39":case "40":case "41":case "42":case "43": case "44":case "45":case "46":case "47":case "48":case "49":case "50":case "51":
            case "91":

                String merchantInfo = payload.substring(lengthStartPosition,payloadLength + lengthStartPosition);

                // Fill merchantInfo Map with <key = Root id, value = template for that id)
                // eg. for payload "0012D156000000000510A93FO3230Q" then < key , Value > < 29 , 0012D156000000000510A93FO3230Q >
                this.merchantInfo.put(id,merchantInfo);

                int GUILength = getLength(merchantInfo);
                String GUI = merchantInfo.substring(lengthStartPosition,GUILength + lengthStartPosition);
                String merchantId = merchantInfo.substring(lengthStartPosition + GUILength).substring(0,2);
                merchantInfo = merchantInfo.substring(lengthStartPosition + GUILength);

                // Fill merchantGUI Map with <key = Root GUI, value = template for that GUI)
                // eg. for payload "0012D156000000000510A93FO3230Q" then < key , Value > < D15600000000 , 0510A93FO3230Q >
                this.merchantGUI.put(GUI, merchantInfo);

                int merchantAccountLength = getLength(merchantInfo);
                String merchantAccount = merchantInfo.substring(lengthStartPosition,merchantAccountLength + lengthStartPosition);

                // Fill merchantGUI Map with <key = Root merchant account id, value = template for that account)
                // eg. for payload "0510A93FO3230Q" then < key , Value > < 05 , A93FO3230Q >
                this.merchantAccount.put(merchantId, merchantAccount);

                payloadLength = 8 + GUILength + lengthStartPosition + merchantAccountLength;

                break;

            case "52": // Merchant Category

                this.MCC = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);
                payloadLength = lengthStartPosition + payloadLength;

                break;

            case "53": // Transaction Currency

                this.transactionCurrency = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);

                payloadLength = lengthStartPosition +payloadLength;

                break;

            case "54": // Transaction Amount

                String amount = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);
                this.transactionAmount = Double.parseDouble(amount);

                payloadLength = lengthStartPosition +payloadLength;

                break;

            case "55": // Tip & Convenience Indicator

                this.tipConvenienceIndicator = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);

                // check tipConvenienceIndicator ID
                if(tipConvenienceIndicator.equalsIgnoreCase("01")) // "01" Tip Prompt
                    this.tipConvenienceIndicator = "tip";
                else if(tipConvenienceIndicator.equalsIgnoreCase("02")) // "02" Fixed Fee
                {
                    this.tipConvenienceIndicator = "fixed fee";
                    payload = payload.substring(6);

                    String fixedFeeId = getID(payload);
                    if(!fixedFeeId.equalsIgnoreCase("56"))
                        throw new Exception("wrong format for Tip Convenience Indicator: ID Must be 56");

                    int feeLength = getLength(payload);

                    String fixedAmount = payload.substring(lengthStartPosition, feeLength + lengthStartPosition);
                    this.fixedFeeValue = Double.parseDouble(fixedAmount);

                    payloadLength = feeLength;
                }
                else if(tipConvenienceIndicator.equalsIgnoreCase("03")) // "03" Percentage Fee
                {
                    this.tipConvenienceIndicator = "Percent Fee";
                    payload = payload.substring(6);

                    String fixedFeeId = getID(payload);
                    if(!fixedFeeId.equalsIgnoreCase("57"))
                        throw new Exception("wrong format for Tip Convenience Indicator: ID Must be 57");

                    int feeLength = getLength(payload);

                    String percentageAmount = payload.substring(lengthStartPosition, feeLength + lengthStartPosition);
                    this.fixedFeeValue = Double.parseDouble(percentageAmount);

                    payloadLength = feeLength;
                }
                payloadLength = lengthStartPosition +payloadLength;

                break;

            case "58": // Country Code

                this.countryCode = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);
                payloadLength = lengthStartPosition + payloadLength;

                break;

            case "59": // Merchant Name

                this.merchantName = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);
                payloadLength = lengthStartPosition + payloadLength;

                break;

            case "60": // Merchant City

                this.merchantCity = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);
                payloadLength = lengthStartPosition +payloadLength;

                break;

            case "62": // Additional Data

                String additionalDataInfo = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);
                do
                {
                    String dataId = getID(additionalDataInfo);

                    // Check Additional Data Payload ID
                    if(dataId.equalsIgnoreCase("01")) // "01" Bill Number
                    {
                        int billNumberLength = getLength(additionalDataInfo);
                        this.billNumber = additionalDataInfo.substring(lengthStartPosition, billNumberLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + billNumberLength);
                    }
                    else if(dataId.equalsIgnoreCase("02")) // "02" Mobile Number
                    {
                        int mobileNumberLength = getLength(additionalDataInfo);
                        this.mobileNumber = additionalDataInfo.substring(lengthStartPosition, mobileNumberLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + mobileNumberLength);
                    }
                    else if(dataId.equalsIgnoreCase("03")) // "03" Store Label
                    {
                        int storeLabelLength = getLength(additionalDataInfo);
                        this.storeLabel = additionalDataInfo.substring(lengthStartPosition, storeLabelLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + storeLabelLength);
                    }
                    else if(dataId.equalsIgnoreCase("04")) // "04" Loyalty Number
                    {
                        int loyaltyNumberLength = getLength(additionalDataInfo);
                        this.loyaltyNumber = additionalDataInfo.substring(lengthStartPosition, loyaltyNumberLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + loyaltyNumberLength);
                    }
                    else if(dataId.equalsIgnoreCase("05")) // "05" Reference Label
                    {
                        int referenceLabelLength = getLength(additionalDataInfo);
                        this.referenceLabel = additionalDataInfo.substring(lengthStartPosition, referenceLabelLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + referenceLabelLength);
                    }
                    else if(dataId.equalsIgnoreCase("06")) // "06" Customer Label
                    {
                        int customerLabelLength = getLength(additionalDataInfo);
                        this.customerLabel = additionalDataInfo.substring(lengthStartPosition, customerLabelLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + customerLabelLength);
                    }
                    else if(dataId.equalsIgnoreCase("07")) // "07" Terminal Label
                    {
                        int terminalLabelLength = getLength(additionalDataInfo);
                        this.terminalLabel = additionalDataInfo.substring(lengthStartPosition, terminalLabelLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + terminalLabelLength);
                    }
                    else if(dataId.equalsIgnoreCase("08")) // "08" Purpose of Transaction Label
                    {
                        int transactionPurposeLength = getLength(additionalDataInfo);
                        this.transactionPurpose = additionalDataInfo.substring(lengthStartPosition, transactionPurposeLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + transactionPurposeLength);
                    }
                    else if(dataId.equalsIgnoreCase("09")) // "09" Consumer Data Label
                    {
                        int consumerLabelLength = getLength(additionalDataInfo);
                        this.consumerData = additionalDataInfo.substring(lengthStartPosition, consumerLabelLength + lengthStartPosition);

                        additionalDataInfo = additionalDataInfo.substring(lengthStartPosition + consumerLabelLength);
                    }
                }
                while (!additionalDataInfo.isEmpty());

                payloadLength = lengthStartPosition + payloadLength  ;

                break;

            case "63": // CRC

                this.CRC = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);
                payloadLength = lengthStartPosition + payloadLength;

                break;

            case "64": // Merchant Language Information

                String languageInfo = payload.substring(lengthStartPosition, payloadLength + lengthStartPosition);

                int languageLength = getLength(languageInfo);
                this.language = languageInfo.substring(lengthStartPosition,languageLength + lengthStartPosition);

                languageInfo = languageInfo.substring(lengthStartPosition + languageLength);

                int nameLength = getLength(languageInfo);
                this.merchantNameWithLanguage = languageInfo.substring(lengthStartPosition, nameLength + lengthStartPosition);

                languageInfo = languageInfo.substring(lengthStartPosition + nameLength);

                int cityLength = getLength(languageInfo);
                this.merchantCity = languageInfo.substring(lengthStartPosition,cityLength + lengthStartPosition);

                payloadLength = 8 + languageLength + lengthStartPosition + nameLength + lengthStartPosition + cityLength;

                break;

            default:
                System.out.println("Wrong Format");
        }

        code = payload.substring(payloadLength);

        return code;
    }

    private String getID(String payload)
    {
        return payload.substring(0,2);
    }

    private Integer getLength(String payload)
    {
        return Integer.parseInt(payload.substring(2,4));
    }
}
