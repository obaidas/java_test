import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateTimeManipulation
{
    public static void main(String[] args)
    {
        LocalDateTime now = LocalDateTime.now();
        System.out.println("Now: \n"+ now +"\n");

        LocalDateTime fromHour = now.of(now.toLocalDate(), LocalTime.of(now.getHour(),0,0));
        System.out.println("from (this hour):\n"+ fromHour +"\n");

        LocalDateTime fromDay = now.of(now.toLocalDate(), LocalTime.MIDNIGHT);
        System.out.println("from (this Day):\n"+ fromDay +"\n");

        LocalDateTime fromMonth = now.withDayOfMonth(1);
        fromMonth = fromMonth.of(fromMonth.toLocalDate(), LocalTime.MIDNIGHT);

        System.out.println("from (this Month):\n"+ fromMonth +"\n");
    }
}
