
import java.util.Scanner;

public class main
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        String userEntry = in.nextLine();

        if(userEntry.equalsIgnoreCase("817"))
        {
            USSDTest ussd = new USSDTest(userEntry);
            while (!userEntry.equalsIgnoreCase("9"))
            {
                Integer level = ussd.getNumberSequence().size();
                String response = "error";

                switch (level)
                {
                    case 0:
                        // Fetch Main Menu and prompt user to enter option
                        System.out.println(ussd.getInitialUSSD());
                        userEntry = in.nextLine();
                        ussd.addLevelToNumberSequence(userEntry);
                        break;

                    case 1: // Level 1

                        if (ussd.getNumberSequence().get(0).equalsIgnoreCase("1")) // Level one ( 1= Digital wallet)
                        {
                            ussd.fetchUSSDSubMenu(); // Prompt to enter Wallet number or cancel
                            userEntry = in.nextLine(); // Enter Wallet Number
                            if(userEntry.equalsIgnoreCase("0"))
                            {
                                ussd.addLevelToNumberSequence("0");
                                break;
                            }

                            response = ussd.callAPI(userEntry); // call wallet number login API

                            if (response.equalsIgnoreCase("success"))// if API is successful push the menu to Level 2
                            {
                                ussd.addLevelToNumberSequence("1"); // select option one inorder to move to level 2 (add one level)
                                break;
                            }
                        }
                        else if(ussd.getNumberSequence().get(0).equalsIgnoreCase("2"))// Level one ( 2= E-Bill)
                        {
                            ussd.fetchUSSDSubMenu(); // Prompt to enter Wallet number or cancel
                            userEntry = in.nextLine(); // Enter Wallet Number
                            if(userEntry.equalsIgnoreCase("0"))
                            {
                                ussd.addLevelToNumberSequence("0");
                                break;
                            }

                            response = ussd.callAPI(userEntry); // call wallet number login API

                            if (response.equalsIgnoreCase("success"))// if API is successful push the menu to Level 2
                            {
                                ussd.addLevelToNumberSequence("1"); // select option one inorder to move to level 2 (add one level)
                                break;
                            }
                        }
                        break;

                    case 2: // Level 2

                        if (ussd.getNumberSequence().get(1).equalsIgnoreCase("1")) // Level 2 ( 1= Enter PIN)
                        {
                            ussd.fetchUSSDSubMenu(); // Prompt to enter PIN or cancel
                            userEntry = in.nextLine(); // Enter PIN
                            if(userEntry.equalsIgnoreCase("0"))
                            {
                                ussd.addLevelToNumberSequence("0");
                                break;
                            }

                            response = ussd.callAPI(userEntry); // call login API

                            if (response.equalsIgnoreCase("success"))// if API is successful push the menu to Level 2
                            {
                                ussd.addLevelToNumberSequence("1"); // select option one inorder to move to level 3 (add one level)
                                break;
                            }
                        }

                    case 3: // Level 3

                        if (ussd.getNumberSequence().get(2).equalsIgnoreCase("1")) // Level 2 ( 1= Check Balance)
                        {
                            ussd.fetchUSSDSubMenu(); // Prompt to check balance
                            userEntry = in.nextLine(); // press 0 to go back
                            if(userEntry.equalsIgnoreCase("0"))
                            {
                                ussd.addLevelToNumberSequence("0");
                                break;
                            }

                            response = ussd.callAPI(userEntry); // call login API

                            if (response.equalsIgnoreCase("success"))// if API is successful push the menu to Level 2
                            {
                                //show balance
                                break;
                            }
                        }
                        else if(ussd.getNumberSequence().get(2).equalsIgnoreCase("2"))// Level one ( 2= Send Money)
                        {
                            ussd.fetchUSSDSubMenu(); // Prompt to select money send method
                            userEntry = in.nextLine(); // press 0 to go back
                            if(userEntry.equalsIgnoreCase("0"))
                            {
                                ussd.addLevelToNumberSequence("0");
                                break;
                            }
                            else
                                ussd.addLevelToNumberSequence(userEntry); // select option one inorder to move to level 3 (add one level)
                                break;

                        }
                            break;

                    default:
                        ussd.fetchUSSDSubMenu();
                        userEntry = in.nextLine();
                        ussd.addLevelToNumberSequence(userEntry);
                        break;
                }
            }
        }
        else
            System.out.println("Error Code: "+userEntry +" ,you must enter code: 817\n\nRun it again please :P");
    }
}

