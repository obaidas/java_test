import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Stack;

public class USSDTest
{
    private Stack<String> numberSequence;
    private String JsonMenu;
    private String InitialUSSD;

    public String getInitialUSSD() {
        return InitialUSSD;
    }

    public void setInitialUSSD(String initialUSSD) {
        InitialUSSD = initialUSSD;
    }

    public String getJsonMenu() {
        return JsonMenu;
    }

    public void setJsonMenu(String jsonMenu) {
        JsonMenu = jsonMenu;
    }

    public Stack<String> getNumberSequence() {
        return numberSequence;
    }

    public void setNumberSequence(Stack<String> numberSequence) {
        this.numberSequence = numberSequence;
    }

    public USSDTest(String number) {
        setNumberSequence(new Stack<>());

        fetchUSSDMenu();
    }

    public USSDTest()
    {
    }

    private String fetchUSSDMenu()
    {
        //String path = environment.getProperty("menu_attachment") + "ussd_menu.json";
        //System.out.println("path" + path);
        //File file = new File(path);

        ClassLoader classLoader = new USSDTest().getClass().getClassLoader();
        File file = new File(classLoader.getResource("ussd_menu.json").getFile());

        String ussdMenuString;
        try {
            String contents = new String(Files.readAllBytes(file.toPath()));
            JSONObject ussdMenuJson = new JSONObject(contents);

            //find JSON Menu
            ussdMenuString = convertToMenu(ussdMenuJson);

            JSONArray firstLevel = ussdMenuJson.getJSONArray("sub");
            setJsonMenu(firstLevel.toString());

        } catch (IOException e) {
            e.printStackTrace();
            return "USSD Service is under maintenance";
        }
        setInitialUSSD(ussdMenuString);

        return ussdMenuString;
    }

    public String fetchUSSDSubMenu()
    {
        String ussdMenuString = getInitialUSSD();
        String subMenu = getJsonMenu();

        JSONArray ussdMenuJsonArray;
        JSONObject subMenuObject;

        Iterator value = getNumberSequence().iterator();
        while (value.hasNext())
        {
            String number = value.next().toString();

            try
            {
                ussdMenuJsonArray = new JSONArray(subMenu);

                subMenuObject = ussdMenuJsonArray.getJSONObject(Integer.valueOf(number)-1);

                ussdMenuString = convertToMenu(subMenuObject);

                subMenu = subMenuObject.getJSONArray("sub").toString();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        //find JSON Menu
        System.out.println(ussdMenuString);

        System.out.println("Number sequence:");
        System.out.println(Arrays.toString(getNumberSequence().toArray()));

        return ussdMenuString;
    }

    public String convertToMenu(JSONObject jsonObject)
    {
        StringBuilder ussdMenu = new StringBuilder();
        ussdMenu.append(jsonObject.getString("text") + "\n");

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("sub");
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject menuItem = jsonArray.getJSONObject(i);
                ussdMenu.append(menuItem.getString("number") + "-" + menuItem.getString("text") + "&#xd;\n");
            }
        }
        catch (Exception e)
        {
            System.out.println("wrong entry");
        }


        return ussdMenu.toString();
    }

    public static String callAPI(String userEntry)
    {
        return "success";
    }

    public Stack<String> addLevelToNumberSequence(String userEntry)
    {
        Stack<String> userEntries = getNumberSequence();
        if(userEntry.equalsIgnoreCase("0"))
        {
            if (!userEntries.isEmpty())
                userEntries.pop();
            else
                System.exit(0);
        }
        else
            userEntries.add(userEntry);

        setNumberSequence(userEntries);

        return userEntries;
    }
}
